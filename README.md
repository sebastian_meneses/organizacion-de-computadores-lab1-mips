# Leeme #

SNAKE MIPS Laboratorio 1 Organización de Computadores.

Implementación del clásico juego Snake en el lenguaje ensamblador de MIPS.

¿Como iniciar?
Abrir el archivo snake.asm con el simulador MARS*
Iniciar el Bitmap Display simulator y conectarlo a MARS
Iniciar el Keyboard and Display MMIO Simulator y conectarlo a Mars
Ejecutar el programa

Contacto:

Sebastián Meneses : sebastian.meneses.n@usach.cl




*Usar el MARS con fix de teclado. En la implementación ofrecida por defecto, el uso en conjunto del bitmap display y el mmio simulator resulta en un cuelgue del simulador MARS. Mas info: https://dtconfect.wordpress.com/2013/02/09/mars-mips-simulator-lockup-hackfix/